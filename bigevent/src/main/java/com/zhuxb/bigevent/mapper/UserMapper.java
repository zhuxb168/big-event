package com.zhuxb.bigevent.mapper;

import com.zhuxb.bigevent.pojo.User;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

@Mapper
public interface UserMapper {

    @Insert("insert into user(username,password,create_time,update_time) values " +
            "(#{userName},#{pwd},now(),now())")
    void register(String userName, String pwd);

    @Select("select * from user where username=#{username}")
    User selectUserByName(String username) ;
}
