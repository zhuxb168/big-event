package com.zhuxb.bigevent.service;

import com.zhuxb.bigevent.pojo.User;


public interface UserService {

    public User register(String userName,String password);

    public User selectUserByName(String username);

    boolean doLogin(String username, String password);
}
