package com.zhuxb.bigevent.service.impl;

import cn.hutool.crypto.SecureUtil;
import com.zhuxb.bigevent.mapper.UserMapper;
import com.zhuxb.bigevent.pojo.User;
import com.zhuxb.bigevent.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserMapper userMapper;

    @Override
    public User register(String userName, String password) {
        //密码加密
        String pwd = SecureUtil.md5(password);
        //存入数据库
        userMapper.register(userName, pwd);
        return null;
    }

    @Override
    public User selectUserByName(String username) {

        return userMapper.selectUserByName(username);
    }

    @Override
    public boolean doLogin(String username, String password) {
        //密码加密
        String pwd = SecureUtil.md5(password);
        //查询用户
        User user = userMapper.selectUserByName(username);
        if (pwd.equals(user.getPassword()))
            return true;
        return false;
    }

}
