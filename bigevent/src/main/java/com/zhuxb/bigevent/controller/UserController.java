package com.zhuxb.bigevent.controller;

import cn.hutool.json.JSONObject;
import cn.hutool.jwt.JWT;
import cn.hutool.jwt.JWTPayload;
import cn.hutool.jwt.JWTUtil;
import com.zhuxb.bigevent.pojo.Result;
import com.zhuxb.bigevent.pojo.User;
import com.zhuxb.bigevent.service.UserService;
import jakarta.validation.constraints.Pattern;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;


@RestController
@RequestMapping("/user")
@Validated
public class UserController {
    @Autowired
    private UserService userService;

    @PostMapping("/register")
    public Result register(@Pattern(regexp = "^\\S{5,16}$") String username, @Pattern(regexp = "^\\S{6,20}$") String password) {
        //判断用户是否存在
        User user = userService.selectUserByName(username);
        if (user == null) {
            //注册用户
            userService.register(username, password);
            return Result.success();
        } else {
            //用户已存在
            return Result.error("用户" + username + "已存在");
        }
    }

    @PostMapping("/login")
    public Result<String> login(@Pattern(regexp = "^\\S{5,16}$") String username, @Pattern(regexp = "^\\S{6,20}$") String password) {
        //判断用户是否存在
        User user = userService.selectUserByName(username);
        if (user == null) {
            return Result.error("用户名或密码错误");
        }
        //用户登录验证
        if (!userService.doLogin(username,password))
            return Result.error("用户名或密码错误");

        Map userMap = new HashMap<>();
        userMap.put("id",user.getId()+"");
        userMap.put("name",user.getUsername());
        userMap.put("expire_time", System.currentTimeMillis() + 1000 * 60 * 60);
        String token = JWTUtil.createToken(userMap,"yulin".getBytes(StandardCharsets.UTF_8));
        System.out.println(token);
        return Result.success(token);
    }

    @GetMapping("/userInfo")
    public Result<User> userInfo(@RequestHeader(name = "Authorization") String token){
        //验证 token信息
        if (!JWTUtil.verify(token,"yulin".getBytes(StandardCharsets.UTF_8)))
            return Result.error("token信息有误");
        /*** 从token中获取用户名 开始 ***/
        JWT jwt = JWTUtil.parseToken(token).setKey("yulin".getBytes(StandardCharsets.UTF_8));
        JSONObject obj = jwt.getPayloads();
        String userName = (String) obj.get("name");
        /*** 从token中获取用户名 结束 ***/

        //根据用户名获取 user 对象
        User user = userService.selectUserByName(userName);
        return Result.success(user);
    }



}
